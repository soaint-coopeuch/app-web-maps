/* eslint-disable no-undef */
import Config from '../../src/config/Config';
    
it('Config BASE_URL: should not be null, empty or undefined', async () => {
    expect(Config.BASE_URL).not.toBeNull();
    expect(Config.BASE_URL).not.toBeUndefined();
    expect(Config.BASE_URL.trim()).not.toEqual('');
});

it('Config MS_PATH: should not be null, empty or undefined', async () => {
    expect(Config.MS_PATH).not.toBeNull();
    expect(Config.MS_PATH).not.toBeUndefined();
    expect(Config.MS_PATH.trim()).not.toEqual('');
});

it('Config BASE_URL_API: should not be null, empty or undefined', async () => {
    expect(Config.BASE_URL_API).not.toBeNull();
    expect(Config.BASE_URL_API).not.toBeUndefined();
    expect(Config.BASE_URL_API.trim()).not.toEqual('');
});

it('Config ENVIRONMENT: should not be null, empty or undefined', async () => {
    expect(Config.ENVIRONMENT).not.toBeNull();
    expect(Config.ENVIRONMENT).not.toBeUndefined();
    expect(Config.ENVIRONMENT.trim()).not.toEqual('');
});
