/* eslint-disable no-undef */
import BandAgentService from '../../src/api/BandAgentService';

it('Band getAll()', async () => {
    const data = await BandAgentService.getAll();
    expect(data).toBeDefined();
    expect(data[0].name).toEqual('Black Sabbath')
})

it('Band get()', async () => {
    const id = 3;
    const data = await BandAgentService.get(id);
    expect(data).toBeDefined();
    expect(data.name).toEqual('Los Tr3s')
})

it('Band create()', async () => {
    const band = {"id": 99, "name": "Ethernia", "description": "Is a heavy metal band from Valparaiso"};
    const data = await BandAgentService.create(band);

    expect(data).toBeDefined();
    expect(data.name).toEqual('Ethernia')
})

it('Band update()', async () => {
    const band = {"id": 7, "name": "Ethernia XX", "description": "Is a heavy metal band from Valparaiso"};
    const data = await BandAgentService.update(band);

    expect(data).toBeDefined();
    expect(data.name).toEqual('Ethernia XX')
})

it('Band delete()', async () => {
    const id = 99;
    const data = await BandAgentService.delete(id);
    expect(data).toBeDefined();
})
