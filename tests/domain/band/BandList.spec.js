/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import BandList from '../../../src/domain/band/BandList';
 
describe('ToDoList component', () => {
  describe('when provided with an empty array of bands', () => {
    it('does not contain any <li> elements', () => {
        const toDoList = shallow(<BandList bandList={[]} />);
        expect(toDoList.find('li').length).toEqual(0);
    })
});
 
  describe('when provided with an array of bands', () => {
    it('contains a matching number of <li> elements', () => {
      const bands = [
        {
          "id": 1,
          "name": "Black Sabbath",
          "description": "Formed in 1968 as the Polka Tulk Blues Band, a blues rock band, the group went through line up changes, renamed themselves as Earth, broke up and reformed"
        },
        {
          "id": 2,
          "name": "Metallica",
          "description": "Is an American heavy metal band. The band was formed in 1981 in Los Angeles, California"
        }];

      const toDoList = shallow(<BandList bandList={bands} />);
      expect(toDoList.find('li').length).toEqual(bands.length);
    })
  });

});