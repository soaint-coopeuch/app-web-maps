/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import Home from '../../../src/domain/pages/Home';
 
describe('#Home test', () => {
  it('contains a span with the Bands text', () => {
    const app = shallow(<Home />);
    expect(app.containsMatchingElement(<span className="title">Bands</span>)).toEqual(true);
  });
});