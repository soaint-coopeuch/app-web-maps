import Api from '../utils/Api';
import Config from '../config/Config';

class BandAgentService {
  constructor() {
    this.url = Config.BASE_URL_API;
    this.api = new Api({ url: this.url });
    this.api.createEntity({ name: 'bands' });
  }

  getAll() {
    return this.api.endpoints.bands
      .getAll()
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error;
      });
  }

  getByParam(_query={ test1: 'val1', test2: 'val2' }, _config={ headers: { 'custom-header-1': 'value', 'custom-header-2': 'value' } }) {
    return this.api.endpoints.bands
      .getAll(_query,_config)
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error;
      });
  }

  get(_id) {
    return this.api.endpoints.bands
      .get({id:_id})
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error;
      });
  }

  create(band) {
    return this.api.endpoints.bands
      .create(band)
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error;
      });
  }

  update(band) {
    return this.api.endpoints.bands
      .update(band)
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error;
      });
  }

  delete(_id) {
    return this.api.endpoints.bands
      .delete({id:_id})
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error;
      });
  }

}

export default new BandAgentService();
