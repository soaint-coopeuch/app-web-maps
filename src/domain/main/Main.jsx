import React from 'react';
import { Route, NavLink, HashRouter } from 'react-router-dom';
import Tracking from '../pages/Tracking';
import Programar from '../pages/Programar';
import Transportista from '../pages/Transportista';
import Historial from '../pages/Historial';

const Main = () => {
  return (
    <HashRouter>
      <div>
        <h1>Sigdo Koppers</h1>
        <ul className="header">
          <li>
            <NavLink exact to="/">Tracking Transportes</NavLink>
          </li>
          <li>
            <NavLink to="/programar">Programar Transporte</NavLink>
          </li>
          <li>
            <NavLink to="/transportista">Gestion de Transportistas</NavLink>
          </li>
          <li>
            <NavLink to="/historial">Historial de Transportes</NavLink>
          </li>
          <li>
            <NavLink to="/estadisticas">Estadísticas</NavLink>
          </li>
        </ul>
        <div className="content">
          <Route exact path="/" component={Tracking} />
          <Route path="/programar" component={Programar} />
          <Route path="/transportista" component={Transportista} />
          <Route path="/historial" component={Historial} />
        </div>
      </div>
      <p>Ambiente: {process.env.NODE_ENV}</p>
    </HashRouter>
  );
};

export default Main;
