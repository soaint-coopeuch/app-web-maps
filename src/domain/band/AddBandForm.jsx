import React from 'react';

const AddBandForm = ({ onInputChange, onFormSubmit }) => (
  <form>
    <div className="form-group">
      <label htmlFor="name">
        Nombre
        <input
          className="form-control"
          name="name"
          onChange={onInputChange}
          placeholder="Destruction"
        />
      </label>
    </div>

    <div className="form-group">
      <label htmlFor="description">
        Descripción
        <textarea
          className="form-control"
          name="description"
          onChange={onInputChange}
          rows="3"
          placeholder="Is a German thrash metal band, formed in 1982 ..."
        />
      </label>
    </div>

    <div className="form-group">
      <label htmlFor="genero">
        Género
        <input
          className="form-control"
          name="genero"
          onChange={onInputChange}
          placeholder="metal"
        />
      </label>
    </div>

    <button className="btn btn-primary" type="submit" onClick={onFormSubmit}>
      {' '}
      Guardar
      {' '}
    </button>
  </form>
);

export default AddBandForm;
