import bandTypes from './bandTypes';
import BandAgentService from '../../api/BandAgentService';

export const addBand = () => {
  return {
    type: bandTypes.ADD_BAND,
  };
};

export const handleInputChange = (name, value) => {
  return {
    type: bandTypes.HANDLE_INPUT_CHANGE,
    payload: { [name]: value },
  };
};

export const toggleBandForm = () => {
  return {
    type: bandTypes.TOGGLE_BAND_FORM,
  };
};

export const fetchBands = () => {
  return async dispatch => {
    try {
      dispatch({ type: 'REQUEST_BANDS', payload: true });
      const bands = await BandAgentService.getAll();
      dispatch({ type: 'RECEIVE_BANDS', payload: bands });
    } catch (error) {
      dispatch({ type: 'ERROR', payload: error });
    }
  };
};
