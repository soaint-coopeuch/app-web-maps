import React, { Component } from 'react';
import { connect } from 'react-redux';
import './band.less';
import AddBand from './AddBand';
import BandList from './BandList';
import { fetchBands } from './bandActions';

class Band extends Component {
  constructor(props) {
    super(props);
    this.returnBandList = this.returnBandList.bind(this);
    const { fetchAllBands } = this.props;
    fetchAllBands();
  }

  returnBandList() {
    const { bandList } = this.props;
    console.log('>>> lista de bandas', bandList);
    return bandList;
  }

  render() {
    return (
      <div>    
        <br />
        <AddBand />

        <br />
        <BandList bandList={this.returnBandList()} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    bandList: state.band.bands.bandList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchAllBands: () => dispatch(fetchBands()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Band);
