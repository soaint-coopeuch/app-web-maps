import { combineReducers } from 'redux';
import initialState from '../../store/initialState';
import bandTypes from './bandTypes';

/* Reductor explícito para los datos del negocio */
const bandReducer = (state = initialState.band.bands, action) => {
  switch (action.type) {
    case bandTypes.ADD_BAND:
      return {
        ...state,
        bandList: [...state.bandList, state.newBand],
      };

    case bandTypes.HANDLE_INPUT_CHANGE:
      return {
        ...state,
        newBand: { ...state.newBand, ...action.payload },
      };

    case bandTypes.REQUEST_BANDS:
      return {
        ...state,
        isFetching: true,
      };

    case bandTypes.RECEIVE_BANDS:
      return {
        ...state,
        isFetching: false,
        bandList: action.payload,
      };

    default:
      return state;
  }
};

/* Reductor explícito para los datos del funcionamiento de la UI */
const uiReducer = (state = initialState.band.ui, action) => {
  switch (action.type) {
    case 'TOGGLE_BAND_FORM': {
      return {
        ...state,
        isAddBandFormHidden: !state.isAddBandFormHidden,
      };
    }
    default:
      return state;
  }
};

const rootbandReducer = combineReducers({
  bands: bandReducer,
  ui: uiReducer,
});

export default rootbandReducer;
