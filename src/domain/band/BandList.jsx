import React from 'react';
import BandCard from './BandCard';

const BandList = props => {
  const { bandList } = props;
  return (
    <ul className="list-group" id="contact-list">
      {bandList.map(band => (
        <li key={band.name} className="list-group-item">
          <BandCard band={band} />
        </li>
      ))}
    </ul>
  );
};

export default BandList;
