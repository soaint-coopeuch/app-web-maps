import React from 'react';
import './band.less';

const BandCard = ({ band }) => {
  return (
    <div>
      <div className="col-xs-8 col-sm-9">
        <p className="name">
          Nombre :
          {' '}
          {band.name}
        </p>
        <p>
          Descripción :
          {' '}
          {band.description}
        </p>
        <p>
          Género :
          {' '}
          {band.genero}
        </p>
      </div>
      <div className="clearfix" />
    </div>
  );
};

export default BandCard;
