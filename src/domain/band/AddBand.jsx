import React, { Component } from 'react';
import { connect } from 'react-redux';
import AddBandForm from './AddBandForm';
import { addBand, handleInputChange, toggleBandForm } from './bandActions';
import Button from '../common/button/Button';

class AddBand extends Component {
  constructor(props) {
    super(props);
    this.showAddBandBox = this.showAddBandBox.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  showAddBandBox() {
    const { onToggle } = this.props;
    onToggle();
  }

  handleInputChange(event) {
    const { target } = event;
    const { value } = target;
    const { name } = target;
    // eslint-disable-next-line no-console
    console.log(name);

    const { onInputChange } = this.props;
    onInputChange(name, value);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { onToggle } = this.props;
    const { onFormSubmit } = this.props;
    onToggle();
    onFormSubmit();
  }

  renderForm() {
    return (
      <div className="col-sm-8 offset-sm-2">
        <AddBandForm
          onFormSubmit={this.handleSubmit}
          onInputChange={this.handleInputChange}
        />
      </div>
    );
  }

  render() {
    const { isHidden } = this.props;

    return (
      <div>
        {isHidden === false ? (
          this.renderForm()
        ) : (
          <Button onClick={this.showAddBandBox} buttonName=" Agregar banda" />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isHidden: state.band.ui.isAddBandFormHidden,
    newBand: state.band.bands.newBand
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFormSubmit: newBand => {
      dispatch(addBand(newBand));
    },
    onInputChange: (name, value) => {
      dispatch(handleInputChange(name, value));
    },

    onToggle: () => {
      dispatch(toggleBandForm());
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddBand);
