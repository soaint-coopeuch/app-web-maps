import React from 'react';

const Programar = () => {
  return (
    <div>
      <h2>Mbote</h2>
      <p>Listado en esperanto:</p>
      <ol>
        <li>Alta konstruaĵo</li>
        <li>Tre maljuna viro</li>
        <li>La malnova ruĝa domo</li>
        <li>Mi neniam fumos</li>
        <li>Mi legis libron foje</li>
      </ol>
    </div>
  );
};

export default Programar;
