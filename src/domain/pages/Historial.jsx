import React from 'react';

const Historial = () => {
  return (
    <div>
      <h2>Historial de transportes</h2>
      <table border="1">
        <tr>
          <th scope="row">ITEM</th>
          <th>EMPRESA</th>
          <th>TIPO CARGA</th>
          <th>MOVIL O DESMOV</th>
          <th>FECHA SOLICITUD</th>
          <th>FECHA SERVICIO</th>
          <th>ORIGEN CARGA</th>
          <th>DESTINO CARGA</th>
          <th>OBSERVACIONES ADICIONALES</th>
          <th>STATUS</th>
        </tr>
        <tr>
          <th>1</th>
          <td>ARTISA</td>
          <td>SUMINISTRO</td>
          <td>MOVILIZACIÓN </td>
          <td>02-08-2021</td>
          <td>02-08-2021</td>
          <td>PROVEEDORES VARIOS</td>
          <td>393</td>
          <td>N/A</td>
          <td>CARGANDO</td>
        </tr>
        <tr>
          <th>2</th>
          <td>IT</td>
          <td>SUMINISTRO</td>
          <td>MOVILIZACIÓN </td>
          <td>02-08-2021</td>
          <td>03-08-2021</td>
          <td>BODEGA IT + DEYSU</td>
          <td>387</td>
          <td>RETIRAR EN DEYSU ADICIONAL</td>
          <td>TRANSITO</td>
        </tr>
        <tr>
          <th>3</th>
          <td>IT</td>
          <td>SUMINISTRO</td>
          <td>DESMOVILIZACION</td>
          <td>03-08-2021</td>
          <td>04-08-2021</td>
          <td>BODEGA IT</td>
          <td>SAMEX STGO</td>
          <td>DURMIENTES</td>
          <td>FINALIZADO</td>
        </tr>
      </table>
    </div>
  );
};

export default Historial;
