import React from 'react';
import { StyledMap } from 'react-openlayers-styled-map';
import { FaCrosshairs } from 'react-icons/fa';
import MyCustomControl from './MyCustomControl';
import './Mapa.css';

const Mapa = () => {
  return (
    <div style={{ display: 'flex'}}>
      <div className="card" style={{ width: '400px'}}>
        <p>Filtros:</p>
        <ol>
          <li>Camion 1</li>
          <li>Camion 2</li>
          <li>Camion 3</li>
          <li>Camion 4</li>
          <li>Camion 5</li>
        </ol>        
      </div>
      <div className="card" style={{ width: '700px', height: '700px' }}>
        <StyledMap
          id='map' 
          icon={<FaCrosshairs size={20} color='#FF00FF' />}
          debugOptions={{ osmBasemap: true }}
          defaultControls={{
            fullScreenMode: {},
            scale: { bar: true, minWidth: 100 },
            zoomButtons: {},
            zoomSlider: {},
          }}
          startCoordinates={[-70.6532253,-33.4445365]}  
          startZoom={13}
        >
          <StyledMap.Controls showRibbon>
            <MyCustomControl styled />
          </StyledMap.Controls>
        </StyledMap>
      </div>
      <div className="card" style={{ width: '400px'}}>
        <div className="titulo">Datos del Transporte:</div>
        <div className="lista">
          <div className="obj">Origen: BODEGA IT</div>
          <div className="obj">Destino: SAMEX STGO</div>
          <div className="obj">Patente: HR-CA-18</div>
          <div className="obj">Hora Salida: 13:15</div>
          <div className="obj">Status: TRANSITO</div>
          <div className="obj">Tipo Carga: SUMINISTRO</div>
          <div className="obj">Observaciones: CONSOLIDADO</div>
        </div>
      </div>
    </div>
  );
};


export default Mapa;
