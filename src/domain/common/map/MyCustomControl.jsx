import React from 'react';
import { Controls } from 'react-openlayers-styled-map';
import { FaCrosshairs } from 'react-icons/fa';
 
const MyCustomControl = () => {
 
  return (
    <Controls.ControlButton
      styled
      icon={<FaCrosshairs size={20} color='#fff' />}
      activeLabel='Print Map Coordinates'
      color='#FE2C54'
      controlKey='PrintMapCoordinates'
    />
  );
};

export default MyCustomControl;