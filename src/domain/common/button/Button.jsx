import React from 'react';

const Button = ({ onClick, buttonName }) => {
  return (
    <button
      className="btn btn-secondary"
      style={{ marginLeft: '15px' }}
      type="submit"
      onClick={onClick}
    >
      {' '}
      {buttonName}
      {' '}
    </button>
  );
};

export default Button;
