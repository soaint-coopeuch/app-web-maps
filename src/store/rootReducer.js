import { combineReducers } from 'redux';
import rootbandReducer from '../domain/band/bandReducer';

const rootReducer = combineReducers({
  band: rootbandReducer,
});

export default rootReducer;
