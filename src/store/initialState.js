const initialState = {
  band: {
    bands: {
      bandList: [],
      newBand: {
        name: '',
        description: '',
      },
    },
    ui: {
      isAddBandFormHidden: true,
    },
  },
  car: {
    cars: {
      carList: [],
      newCar: {
        model: '',
        year: '',
      },
    },
    ui: {
      isAddCarFormHidden: true,
    },
  },
};

export default initialState;
